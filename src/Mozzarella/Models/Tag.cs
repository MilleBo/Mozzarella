﻿namespace Mozzarella.Web.Models
{
    public class Tag
    {
        public int Id { get; set; }

        public string Text { get; set; }
    }
}