﻿using System.Collections.Generic;

namespace Mozzarella.Web.Models
{
    public class Branch
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public int AuthorId { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}