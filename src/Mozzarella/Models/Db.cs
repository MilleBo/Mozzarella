﻿using System.Data.Entity;

namespace Mozzarella.Web.Models
{
    public class Db : DbContext
    {
        public Db()
            : base("Db")
        {
            Database.SetInitializer<Db>(new DatabaseInitializer());
        }

        public DbSet<Thread> Threads { get; set; }

        public DbSet<Branch> Branchs { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Tag> Tags { get; set; }
    }
}