﻿namespace Mozzarella.Web.Models
{
    public class Post
    {
        public int Id { get; set; }

        public string AuthorId { get; set; }

        public string Text { get; set; }
    }
}