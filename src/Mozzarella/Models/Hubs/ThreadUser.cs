﻿namespace Mozzarella.Web.Models
{
    public class ThreadUser
    {
        public string Name { get; set; }

        public int ThreadId { get; set; }

        public int BranchId { get; set; }
    }
}