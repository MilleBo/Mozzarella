﻿namespace Mozzarella.Web.Models.Hubs
{
    public class ThreadSummary
    {
        public int ThreadId { get; set; }

        public string Title { get; set; }
    }
}