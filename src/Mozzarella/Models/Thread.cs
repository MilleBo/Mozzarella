﻿using System.Collections.Generic;

namespace Mozzarella.Web.Models
{
    public class Thread
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public int AuthorId { get; set; }

        public virtual ICollection<Branch> Branches { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
    }
}