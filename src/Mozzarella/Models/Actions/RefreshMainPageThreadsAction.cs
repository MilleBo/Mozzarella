﻿using System.Collections.Generic;
using Mozzarella.Web.Models.Hubs;

namespace Mozzarella.Web.Models.Actions
{
    public class RefreshMainPageThreadsAction : Action
    {
        public override ActionTypes Type => ActionTypes.RefreshMainPageThreads;

        public IList<ThreadSummary> Threads { get; set; }
    }
}