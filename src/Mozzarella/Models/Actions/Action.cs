﻿namespace Mozzarella.Web.Models.Actions
{
    public abstract class Action
    {
        public abstract ActionTypes Type { get; }
    }
}