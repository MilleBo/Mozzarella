﻿namespace Mozzarella.Web.Models.Actions
{
    public class NewPostAction : Action
    {
        public string Author { get; set; }

        public string Text { get; set; }

        public int BranchId { get; set; }

        public override ActionTypes Type => ActionTypes.NewPost;
    }
}