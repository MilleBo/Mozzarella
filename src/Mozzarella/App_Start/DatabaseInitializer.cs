﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Mozzarella.Web.Models;

namespace Mozzarella
{
    public class DatabaseInitializer : DropCreateDatabaseIfModelChanges<Db>
    {
        protected override void Seed(Db context)
        {
            var tag = new Tag {Id = 1, Text = "#MyThreadOwn"};
            var post = new Post {Id = 1, AuthorId = "Dan", Text = "I don't like your thread."};
            var branch = new Branch {AuthorId = 1, Posts = new List<Post> { post }, Title = null};

            context.Tags.Add(tag);
            context.Posts.Add(post);
            context.Branchs.Add(branch);
            context.Threads.Add(new Thread
            {
                Id = 1,
                AuthorId = 1,
                Branches = new List<Branch> { branch },
                Title = "My thread",
                Tags = new List<Tag> { tag },
                Text = "Do you like my thread?"
            });

            base.Seed(context);
        }
    }
}