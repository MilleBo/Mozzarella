﻿namespace Mozzarella.Web.Models
{
    public enum ActionTypes
    {
        NewPost,
        RefreshMainPageThreads
    }
}