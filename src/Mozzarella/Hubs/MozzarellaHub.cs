﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Mozzarella.Web.Hubs.States;
using Mozzarella.Web.Models;
using Mozzarella.Web.Models.Actions;
using Mozzarella.Web.Models.Hubs;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Mozzarella.Web.Hubs
{
    public class MozzarellaHub : Hub
    {
        public void JoinThread(int threadId)
        {
            ThreadState.State.Connect(Context.ConnectionId, Context.User, threadId);
        }

        public void Post(string message)
        {
            ThreadState.State.Post(Context.ConnectionId, message);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            ThreadState.State.Disconnect(Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public void GetThreads()
        {
            using (var db = new Db())
            {
                var threads = db.Threads.Select(t => new ThreadSummary() { ThreadId = t.Id, Title = t.Title });
                Clients.Caller.Dispatch(JsonConvert.SerializeObject(
                    new RefreshMainPageThreadsAction { Threads = threads.ToList() },
                    new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter> { new Newtonsoft.Json.Converters.StringEnumConverter() } }));
            }
        }

    }
}