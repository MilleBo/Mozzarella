﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Security.Principal;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Mozzarella.Web.Models;
using Mozzarella.Web.Models.Actions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Mozzarella.Web.Hubs.States
{
    public class ThreadState
    {
        private static readonly Lazy<ThreadState> Instance = new Lazy<ThreadState>(() => new ThreadState(GlobalHost.ConnectionManager.GetHubContext<MozzarellaHub>()));
        private readonly ConcurrentDictionary<string, ThreadUser> _users;

        public ThreadState(IHubContext context)
        {
            _users = new ConcurrentDictionary<string, ThreadUser>();
            Clients = context.Clients;
            Groups = context.Groups;
        }

        public static ThreadState State => Instance.Value;

        public IHubConnectionContext<dynamic> Clients { get; set; }

        public IGroupManager Groups { get; set; }

        public void Connect(string connectionId, IPrincipal user, int threadId)
        {
            // TODO: Grab thread from database and get main branch id
            var branchId = 1234;
            var createdUser = CreateUser(threadId, branchId);
            _users.TryAdd(connectionId, createdUser);
            Groups.Add(connectionId, threadId.ToString());
        }

        public void Disconnect(string connectionId)
        {
            ThreadUser user;
            _users.TryRemove(connectionId, out user);
            Groups.Remove(connectionId, user.ThreadId.ToString());
        }

        public void Post(string connectionId, string message)
        {
            var user = _users[connectionId];
            var settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Converters = new List<JsonConverter> {new Newtonsoft.Json.Converters.StringEnumConverter() }
            };

            Clients.Group(user.ThreadId.ToString()).Dispatch(JsonConvert.SerializeObject(
                new NewPostAction { Author = user.Name, Text = message, BranchId = user.BranchId },
                settings));
        }

        private ThreadUser CreateUser(int threadId, int branchId)
        {
            return new ThreadUser { Name = "DAN_LOVE_NUGET", ThreadId = threadId, BranchId = branchId};
        }
    }
}