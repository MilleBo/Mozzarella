//import { Provider } from 'react-redux';
//import { createStore } from 'redux';
//import todoApp from './reducers';
//import App from './components/App';
console.log('test');
/*
REDUCERS
*/
var Screens;
(function (Screens) {
    Screens[Screens["MainPage"] = 0] = "MainPage";
    Screens[Screens["Thread"] = 1] = "Thread";
})(Screens || (Screens = {}));
var ActionTypes;
(function (ActionTypes) {
    ActionTypes[ActionTypes["NewPost"] = 0] = "NewPost";
    ActionTypes[ActionTypes["RefreshMainPageThreads"] = 1] = "RefreshMainPageThreads";
    ActionTypes[ActionTypes["Error"] = 2] = "Error";
})(ActionTypes || (ActionTypes = {}));
const initialState = {
    screen: Screens.MainPage,
    log: [],
    currentThreadId: null,
    currentBranchId: null,
    mainPageThreadIds: [],
    threads: {},
};
;
function mozzarellaApp(state = initialState, action) {
    state = Object.assign({}, state, {
        log: [
            ...state.log,
            {
                date: new Date(),
                action: action,
            },
        ],
    });
    switch (action.type) {
        case 'NewPost':
            return state;
        case 'Error':
        default:
            return state;
    }
}
// APP
const mapStateToProps = state => {
    return state;
};
const mapDispatchToProps = dispatch => {
    return {
        onClick: id => {
            console.log('TODO dispatch', id);
        },
    };
};
const App = (props) => (React.createElement("div", null,
    React.createElement("h1", null, props.screen),
    JSON.stringify(props),
    React.createElement("div", null, "what's here?"),
    JSON.stringify(Object.keys(props)),
    React.createElement("hr", null),
    React.createElement(Log, { log: props.log })));
const ReduxedApp = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(App);
// LOG
function formatLog({ date, action }) {
    return `${JSON.stringify(date)} ${action.type}: ${JSON.stringify(action)}`;
}
const Log = (props) => (React.createElement("pre", null, props.log.map(x => formatLog(x)).join('\n')));
/// INDEX
const store = Redux.createStore(mozzarellaApp);
console.log('got', store.getState());
ReactDOM.render(React.createElement(ReactRedux.Provider, { store: store },
    React.createElement(ReduxedApp, null)), document.getElementById('root'));
function parseAction(action) {
    try {
        return JSON.parse(action);
    }
    catch (e) {
        console.error(e);
        return {
            type: 'Error',
        };
    }
}
$.connection.mozzarellaHub.client.dispatch = function (stringAction) {
    const action = parseAction(stringAction);
    store.dispatch(action);
};
$.connection.hub.start().done(function () {
    $.connection.mozzarellaHub.server.joinThread(1234);
    $.connection.mozzarellaHub.server.getThreads();
    setTimeout(() => {
        $.connection.mozzarellaHub.server.post("halloj");
    }, 3000);
});
//# sourceMappingURL=index.js.map