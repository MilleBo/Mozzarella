﻿interface SignalR {
    mozzarellaHub: any,
}
declare const React: any;
declare const ReactDOM: any;
declare const ReactRedux: any;

//import { Provider } from 'react-redux';
//import { createStore } from 'redux';
//import todoApp from './reducers';
//import App from './components/App';


console.log('test');

/*
REDUCERS
*/
enum Screens {
    MainPage,
    Thread,
}

enum ActionTypes {
    NewPost,
    RefreshMainPageThreads,
    Error,
}

const initialState = {
    screen: Screens.MainPage,

    log: [],

    currentThreadId: null,
    currentBranchId: null,

    mainPageThreadIds: [],
    threads: {
    },
};
interface IAction {
    type: (
        'NewPost'
        | 'Error'
    )
};
function mozzarellaApp(state = initialState, action: IAction) {
    state = Object.assign({}, state, {
        log: [
            ...state.log,
            {
                date: new Date(),
                action: action,
            },
        ],
    });

    switch (action.type) {
        case 'NewPost':
            return state;

        case 'Error':
        default:
            return state;
    }
}

// APP
const mapStateToProps = state => {
    return state;
};

const mapDispatchToProps = dispatch => {
    return {
        onClick: id => {
            console.log('TODO dispatch', id);
        },
    };
};

const App = (props) => (
    <div>
        <h1>{props.screen}</h1>

        {JSON.stringify(props)}
        <div>what's here?</div>
        {JSON.stringify(Object.keys(props))}

        <hr />
        <Log log={props.log} />
    </div>
);

const ReduxedApp = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(App);


// LOG
function formatLog({ date, action }) {
    return `${JSON.stringify(date)} ${action.type}: ${JSON.stringify(action)}`;
}
const Log = (props) => (
    <pre>
        {props.log.map(x => formatLog(x)).join('\n')}
    </pre>
);


/// INDEX
const store = Redux.createStore(mozzarellaApp);

console.log('got', store.getState());

ReactDOM.render(
    <ReactRedux.Provider store={store}>
        <ReduxedApp />
    </ReactRedux.Provider>,
    document.getElementById('root')
);


function parseAction(action: string): IAction {
    try {
        return JSON.parse(action);
    } catch (e) {
        console.error(e);
        return {
            type: 'Error',
            //message: e.message,
            //data: e,
        };
    }
}

$.connection.mozzarellaHub.client.dispatch = function (stringAction: string) {
    const action = parseAction(stringAction);
    store.dispatch(action);
};

$.connection.hub.start().done(function () {
    $.connection.mozzarellaHub.server.joinThread(1234);
    $.connection.mozzarellaHub.server.getThreads();

    setTimeout(() => {
        $.connection.mozzarellaHub.server.post("halloj");
    }, 3000);
});
